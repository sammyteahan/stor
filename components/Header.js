import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const Header = props => (
  <View style={{ justifyContent: 'center', backgroundColor: 'transparent' }}>
    <Image
      style={{ width: 100, height: 100 }}
      source={require('../assets/images/stor-logo.png')}
    />
  </View>
);

const ImageHeader = props => (
  <View style={{ backgroundColor: '#eee' }}>
    <Image
      style={{ width: 10 }}
      source={require('../assets/images/stor-logo.png')}
    />
  </View>
);

export { Header as default, ImageHeader };
