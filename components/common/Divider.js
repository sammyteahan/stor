import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Colors, UIVariables } from '../../constants';

const Divider = ({ style, ...rest }) => (
  <View style={[styles.divider, style]} {...rest} />
);

const styles = StyleSheet.create({
  divider: {
    height: UIVariables.Components.BorderHeight,
    backgroundColor: Colors.Backgrounds.Gray,
  },
});

export { Divider as default };
