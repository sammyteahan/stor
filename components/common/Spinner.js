import React from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';

import { Colors, UIVariables } from '../../constants';

const Spinner = ({ size, ...rest }) => (
  <ActivityIndicator size="large" {...rest} />
);

Spinner.defaultProps = {
  size: 'large',
  color: '#777777',
};

const SpinnerOverlay = () => (
  <View style={styles.overlay}>
    <View style={styles.spinnerContainer}>
      <Spinner color={Colors.Backgrounds.White} />
    </View>
  </View>
);

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    backgroundColor: Colors.Backgrounds.Transparent,
    alignItems: 'center',
    justifyContent: 'center',
  },

  spinnerContainer: {
    backgroundColor: Colors.Backgrounds.Gray,
    padding: UIVariables.Padding.Large,
    borderRadius: UIVariables.Components.BorderRadius,
  },
});

export { Spinner as default, SpinnerOverlay };
