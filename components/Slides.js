import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Colors, UIVariables } from '../constants';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Slides extends Component {
  renderNavigationButton(index) {
    if (index === this.props.data.length - 1) {
      return (
        <TouchableOpacity style={styles.button} onPress={this.props.onNavigate}>
          <Text style={{ color: '#fff', fontSize: 18 }}>Onwards!</Text>
        </TouchableOpacity>
      );
    }
  }

  renderSlides() {
    return this.props.data.map((slide, index) => {
      return (
        <View key={slide.text} style={[styles.slide, { backgroundColor: slide.color }]}>
          <Text style={styles.slideText}>{slide.text}</Text>
          {this.renderNavigationButton(index)}
        </View>
      );
    });
  }

  render() {
    return (
      <ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        {this.renderSlides()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: SCREEN_WIDTH,
  },
  slideText: {
    fontSize: 30,
    color: Colors.Backgrounds.White,
  },
  button: {
    padding: UIVariables.Padding.Large,
    marginTop: UIVariables.Padding.Large,
    backgroundColor: '#2980b9',
    width: '50%',
    alignSelf: 'center',
    alignItems: 'center',
  },
});

export { Slides as default };
