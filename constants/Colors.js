const tintColor = '#2f95dc';

const rgba = (red, green, blue, alpha = 1) => `rgba(${red}, ${green}, ${blue}, ${alpha})`;

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  Backgrounds: {
    Brand: '#2ecc71', // main green
    Gray: rgba(204, 204, 204), // #ccc
    LightGray: rgba(238, 238, 238), // #eee
    DarkGray: rgba(153, 153, 153), // #999
    White: rgba(255, 255, 255),
    DarkText: rgba(96, 100, 109),
    White: rgba(255, 255, 255), // #fff
    Transparent: 'transparent',
  },
};
