import Colors from './Colors';
import UIVariables from './UIVariables';

export {
  Colors,
  UIVariables,
};
