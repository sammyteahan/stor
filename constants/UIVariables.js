const { freeze } = Object;

const Spacing = freeze({
  Small: 5,
  Normal: 10,
  Large: 15,
});


const UIVariables = freeze({
  FontFamily: {
    Header: 'HelveticaNeue-Bold',
    HeaderLight: 'HelveticaNeue-Thin',
    Body: 'HelveticaNeue',
  },

  Components: {
    BorderHeight: 0.5,
    BorderRadius: 5,
  },

  Padding: freeze({ ...Spacing }),
  Margin: freeze({ ...Spacing }),
});

export { UIVariables as default };
