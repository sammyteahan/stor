import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom, StackNavigator } from 'react-navigation';

import Colors from '../constants/Colors';

import WelcomeScreen from '../screens/WelcomeScreen';
import HomeScreen from '../screens/HomeScreen';
import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryScreen from '../screens/CategoryScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ReceiptScreen from '../screens/ReceiptScreen';

const CategoryNavigator = StackNavigator({
  categories: { screen: CategoriesScreen },
  category: { screen: CategoryScreen },
}, { headerMode: 'none' });

const ModalNavigator = StackNavigator({
  categories: { screen: CategoryNavigator },
  receipt: { screen: ReceiptScreen },
}, {
  mode: 'modal',
  headerMode: 'none',
  cardStyle: {
    backgroundColor: 'transparent',
    position: 'absolute',
  },
});

export default TabNavigator({
  welcome: { screen: WelcomeScreen },
  home: {
    screen: TabNavigator({
      home: { screen: HomeScreen },
      categories: { screen: ModalNavigator},
    }, {
      tabBarPosition: 'bottom',
      tabBarOptions: {
        labelStyle: { fontSize: 12 },
      },
    }),
  },
}, {
  navigationOptions: {
    tabBarVisible: false,
  },
  lazyLoad: true,
});
