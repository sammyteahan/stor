import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Colors, UIVariables } from '../constants';
import { Ionicons } from '@expo/vector-icons';

const { width } = Dimensions.get('window');

export default class ReceiptScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Receipt',
    tabBarLabel: 'Categories',
    tabBarIcon: ({ focused }) => {
      return (
        <Ionicons
          name='ios-archive'
          style={{ marginBottom: -3 }}
          size={28}
          color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
      );
    },
    headerRight: (
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <View hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}>
          <Ionicons
            name='ios-close'
            style={{ paddingRight: UIVariables.Padding.Normal }}
            size={28}
            color={Colors.Backgrounds.DarkText}
          />
        </View>
      </TouchableOpacity>
    ),
  });

  renderHeader() {
    const { params } = this.props.navigation.state;

    return (
      <View style={styles.receiptHeader}>
        <Text style={styles.receiptTitle}>{params.item.store}</Text>
        <View style={styles.checkmarkWrapper}>
          <Ionicons
            name='ios-checkmark'
            size={50}
            color={Colors.Backgrounds.Brand}
          />
        </View>
      </View>
    );
  }

  renderPurchases() {
    const data = [
      { id: '1', title: 'Half Baked', price: '21.00' },
      { id: '2', title: 'Basketball', price: '14.99' },
      { id: '3', title: 'Gatorade', price: '16.32' },
      { id: '4', title: 'Rockstar', price: '5.50' },
      { id: '5', title: 'Bagel', price: '14.99' },
      { id: '6', title: 'Apples', price: '82.21' },
      { id: '7', title: 'Greek Yogurt', price: '90.00' },
      { id: '8', title: 'Paper Towels', price: '7.99' },
      { id: '9', title: 'Smoked Gouda', price: '4.99' },
      { id: '10', title: 'Chips', price: '3.29' },
    ];

    return data.map((receipt) => {
      return (
        <View key={receipt.id} style={styles.row}>
          <View style={styles.col}><Text style={styles.content}>{receipt.title}</Text></View>
          <View style={[styles.col, { alignItems: 'flex-end' }]}><Text style={styles.content}>{receipt.price}</Text></View>
        </View>
      )
    });
  }

  renderPayment() {
    return (
      <View style={styles.row}>
        <View style={styles.col}><Text style={styles.content}>Visa</Text></View>
        <View style={[styles.col, { alignItems: 'flex-end' }]}><Text style={styles.content}>*** *** **** 4242</Text></View>
      </View>
    );
  }

  renderReceiptBody() {
    const { params } = this.props.navigation.state;

    return (
      <View style={styles.receiptBody}>
        <View style={styles.row}>
          <View style={styles.col}>
            <Text style={styles.mutedTitle}>Date</Text>
            <Text style={styles.content}>{params.item.day} {params.item.month} {params.item.year}</Text>
          </View>
          <View style={styles.col}>
            <Text style={styles.mutedTitle}>Ref</Text>
            <Text style={styles.content}>135625425</Text>
          </View>
        </View>
        <View style={[styles.row, styles.spacerLarge]}></View>

        <View style={styles.row}>
          <View style={styles.col}>
            <Text style={[styles.mutedTitle, { fontSize: 18 }]}>Purchases</Text>
          </View>
        </View>
        <View style={[styles.row, styles.spacerSmall]}></View>
        {this.renderPurchases()}
        <View style={[styles.row, styles.spacerSmall]}></View>
        <View style={styles.row}>
          <View style={styles.col}>
            <Text style={[styles.mutedTitle, { fontSize: 18 }]}>Payment</Text>
          </View>
        </View>
        <View style={[styles.row, styles.spacerSmall]}></View>
        {this.renderPayment()}
        <View style={[styles.row, styles.spacerSmall]}></View>
      </View>
    );
  }

  renderFooter() {
    return (
      <View style={styles.receiptFooter}>
        <View style={[styles.row, { justifyContent: 'space-between', alignItems: 'center' }]}>
          <Text style={[styles.mutedTitle, { fontSize: 22 }]}>Total:</Text>
          <Text style={[styles.content, styles.totalPrice, { fontSize: 20 }]}>$261.28</Text>
        </View>
      </View>
    );
  }

  render() {
    const { params } = this.props.navigation.state;

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.receiptWrapper}>
            {this.renderHeader()}
            {this.renderReceiptBody()}
            {this.renderFooter()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Backgrounds.White,
    alignItems: 'center',
  },
  receiptWrapper: {
    marginTop: UIVariables.Margin.Normal,
    marginBottom: UIVariables.Margin.Normal,
    width: (width - 30),
  },
  receiptHeader: {
    backgroundColor: Colors.Backgrounds.Brand,
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,
    alignItems: 'center',
    padding: UIVariables.Padding.Large,
    height: 200,
  },
  receiptTitle: {
    fontSize: 32,
    color: Colors.Backgrounds.White,
    marginBottom: UIVariables.Margin.Large,
  },
  checkmarkWrapper: {
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: Colors.Backgrounds.White,
    alignItems: 'center',
    justifyContent: 'center',
  },
  receiptBody: {
    backgroundColor: Colors.Backgrounds.LightGray,
    padding: UIVariables.Padding.Large,
  },
  row: {
    flexDirection: 'row',
  },
  col: {
    flex: 1,
  },
  spacerLarge: {
    margin: UIVariables.Margin.Large,
  },
  spacerSmall: {
    margin: UIVariables.Margin.Small,
  },
  mutedTitle: {
    fontWeight: 'bold',
    color: Colors.Backgrounds.DarkGray,
  },
  content: {
    fontSize: 18,
    color: Colors.Backgrounds.DarkText,
  },
  receiptFooter: {
    borderTopColor: Colors.Backgrounds.Gray,
    borderTopWidth: 2,
    borderBottomRightRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: Colors.Backgrounds.LightGray,
    padding: UIVariables.Padding.Large,
  },
  totalPrice: {
    fontWeight: 'bold',
    color: Colors.Backgrounds.Brand,
  }
});
