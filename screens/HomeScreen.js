import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';
import { Ionicons, EvilIcons } from '@expo/vector-icons';

import { Card, CardSection, Divider } from '../components/common';
import { Colors, UIVariables } from '../constants';
import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
    title: 'Home',
    tabBarIcon: ({ focused }) => {
      return (
        <Ionicons
          name='ios-home'
          style={{ marginBottom: -3 }}
          size={28}
          color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
      );
    }
  };

  render() {
    const { navigation: { navigate, goBack, push, pop } } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.logoWrapper}>
            <Image
              source={require('../assets/images/stor-logo.png')}
              style={styles.welcomeImage}
            />
          </View>

          <Card>
            <CardSection>
              <Text style={styles.cardTitle}>Store a receipt</Text>
            </CardSection>
            <CardSection>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Ionicons
                  name='ios-camera'
                  size={72}
                  color={Colors.Backgrounds.Brand}
                />
              </View>
            </CardSection>
          </Card>

          <Card>
            <CardSection>
              <Text style={styles.cartTitle}>Navigation test</Text>
            </CardSection>
            <CardSection>
                <TouchableOpacity onPress={() => goBack()} style={styles.testLinkWrapper}>
                  <Text>Test</Text>
                  <EvilIcons
                    name="question"
                    size={32}
                    color={Colors.Backgrounds.Brand}
                  />
                </TouchableOpacity>
            </CardSection>
          </Card>

          <Card>
            <CardSection>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={styles.cardTitle}>Manage your categories</Text>
                <Ionicons
                  name="md-add"
                  size={28}
                  color="#000"
                  style={{ paddingRight: UIVariables.Padding.Small }}
                />
              </View>
            </CardSection>
            <CardSection>
              <View>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-card' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Finance</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-plane' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Business Receipts</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-ice-cream' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Fun Money</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </CardSection>
          </Card>

          <Card>
            <CardSection>
              <Text style={styles.cardTitle}>Advertisement</Text>
            </CardSection>
            <CardSection>
              <View style={{ flex: 1 }}>
              <Image
                source={{ uri: 'http://via.placeholder.com/350x150'}}
                style={{ flex: 1, width: null, height: 150 }}
              />
              </View>
            </CardSection>
          </Card>

          <Card style={{ marginBottom: UIVariables.Padding.Normal }}>
            <CardSection>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={styles.cardTitle}>Our suggestions</Text>
              </View>
            </CardSection>
            <CardSection>
              <View>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-basket' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Food</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-battery-full' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Technology</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.listItemWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Ionicons name='ios-hammer' size={22} color={Colors.Backgrounds.DarkText} />
                      <Text style={styles.listItem}>Home Expenses</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </CardSection>
          </Card>
        </ScrollView>
      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Backgrounds.White,
  },
  contentContainer: {
    paddingTop: 30,
  },
  logoWrapper: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  cardTitle: {
    fontSize: 22,
    color: Colors.Backgrounds.DarkText,
  },
  listItemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.Backgrounds.White,
    paddingVertical: UIVariables.Padding.Normal,
    paddingHorizontal: UIVariables.Padding.Large,
  },
  listItem: {
    color: Colors.Backgrounds.DarkText,
    paddingLeft: UIVariables.Padding.Normal,
  },
  testLinkWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});
