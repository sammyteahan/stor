import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';
import { ExpoConfigView } from '@expo/samples';
import { UIVariables, Colors } from '../constants/';

import Slides from '../components/Slides';

const SLIDE_DATA = [
  { text: 'Welcome to STOR', color: '#2ecc71' },
  { text: "We store your receipts so you don't have to", color: '#009688' },
  { text: 'Get started', color: '#03A9F4' },
];

export default class WelcomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  /**
   * @todo set token in AsyncStorage
   */
  onNavigate = () => {
    this.props.navigation.navigate('home');
  }

  render() {
    return (
      <Slides
        data={SLIDE_DATA}
        onNavigate={this.onNavigate}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: Colors.Backgrounds.Brand,
  },
  welcomeText: {
    fontSize: 32,
    color: 'white',
    fontFamily: UIVariables.FontFamily.Body,
    alignSelf: 'center',
  },
  button: {
    padding: UIVariables.Padding.Large,
    backgroundColor: '#27ae60',
    width: '50%',
    alignSelf: 'center',
    alignItems: 'center',
  },
});
