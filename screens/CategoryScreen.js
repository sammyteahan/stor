import React from 'react';
import { TouchableOpacity, View, ScrollView, FlatList, StyleSheet, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Colors, UIVariables } from '../constants';
import Header, { ImageHeader } from '../components/Header';
import { Divider, SpinnerOverlay } from '../components/common';
import receipt_data from '../constants/receipt_data';

export default class CategoryScreen extends React.Component {
  static navigationOptions = ({ navigation: { goBack } }) => ({
    title: 'Category',
    headerLeft: (
      <TouchableOpacity onPress={() => goBack()}>
        <View hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}>
          <Ionicons
            name='ios-arrow-back'
            style={{ paddingLeft: UIVariables.Padding.Normal }}
            size={28}
            color={Colors.Backgrounds.DarkText}
          />
        </View>
      </TouchableOpacity>
    ),
    // tabBarVisible: false, // Nice, pretty easy to hide the tab bar
    tabBarLabel: 'Categories',
    tabBarIcon: ({ focused }) => {
      return (
        <Ionicons
          name='ios-archive'
          style={{ marginBottom: -3 }}
          size={28}
          color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
      );
    }
  });

  constructor(props) {
    super(props);

    this.state = { loading: true };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 500);
  }

  renderLoading() {
    return <SpinnerOverlay />;
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('receipt', { item })}>
        <View style={styles.listItemWrapper}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
              <Text style={{ fontSize: 10, color: Colors.Backgrounds.DarkText }}>{item.month}</Text>
              <Text style={{ fontSize: 18, color: Colors.Backgrounds.DarkText }}>{item.day}</Text>
            </View>
            <Text style={styles.listItem}>{item.store}</Text>
          </View>
        </View>
        <Divider />
      </TouchableOpacity>
    );
  }

  renderContent() {
    const { params } = this.props.navigation.state;

    return (
      <View style={{ paddingBottom: 70 }}>
        <Text style={{ fontSize: 32, padding: UIVariables.Padding.Large }}>{params.item.name}</Text>
        <FlatList
          data={receipt_data}
          renderItem={this.renderItem}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? this.renderLoading() : this.renderContent() }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Backgrounds.LightGray,
  },
  listItemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.Backgrounds.White,
    paddingVertical: UIVariables.Padding.Normal,
    paddingHorizontal: UIVariables.Padding.Large,
  },
  listItem: {
    color: Colors.Backgrounds.DarkText,
    paddingLeft: UIVariables.Padding.Large,
  },
});
