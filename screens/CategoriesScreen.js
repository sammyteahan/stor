import React from 'react';
import { TouchableOpacity, View, ScrollView, FlatList, StyleSheet, Text } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { Ionicons } from '@expo/vector-icons';
import { Colors, UIVariables } from '../constants';
import Header, { ImageHeader } from '../components/Header';
import { Divider } from '../components/common';

export default class CategoriesScreen extends React.Component {
  static navigationOptions = {
    title: 'Categories',
    tabBarIcon: ({ focused }) => {
      return (
        <Ionicons
          name='ios-archive'
          style={{ marginBottom: -3 }}
          size={28}
          color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
      );
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      categories: [
        {id: 1, key: 1, name: 'Shopping', icon: 'ios-pricetag-outline'},
        {id: 2, key: 2, name: 'Groceries', icon: 'ios-cart-outline'},
        {id: 3, key: 3, name: 'Gas', icon: 'ios-car-outline'},
        {id: 4, key: 4, name: 'Food', icon: 'ios-pizza-outline'},
        {id: 5, key: 5, name: 'Tech', icon: 'ios-laptop-outline'},
        {id: 6, key: 6, name: 'Insurance', icon: 'ios-clipboard-outline'},
        {id: 7, key: 7, name: 'Business', icon: 'ios-briefcase-outline'},
        {id: 8, key: 8, name: 'Misc.', icon: 'ios-flask-outline'},
      ],
    };
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('category', { item })}>
        <View style={styles.listItemWrapper}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Ionicons name={item.icon} size={22} color={Colors.Backgrounds.DarkText} />
            <Text style={styles.listItem}>{item.name}</Text>
          </View>
          <Ionicons
            name='ios-arrow-forward'
            size={28}
            color={Colors.Backgrounds.Gray}
          />
        </View>
        <Divider />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <FlatList
          data={this.state.categories}
          renderItem={this.renderItem}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Backgrounds.LightGray,
    paddingTop: UIVariables.Padding.Large,
  },
  listItemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.Backgrounds.White,
    paddingVertical: UIVariables.Padding.Normal,
    paddingHorizontal: UIVariables.Padding.Large,
  },
  listItem: {
    color: Colors.Backgrounds.DarkText,
    paddingLeft: UIVariables.Padding.Normal,
  },
});
